# fitness-typescript

## Name

Fitness-app using React and Typescript

## Description

The goal of the project is to make use of React and Typescript and build a fitness web applicaiton making it response with the use of Tailwind-css

## Installation

### Prerequisites

- Node Version: v16.14.2
- NPM Version: 6.14.16

### Step 1: Clone the repository

```
cd existing_repo
git remote add origin https://gitlab.com/manishsudeshkumar/fitness-typescript.git
```

### Step 2: Install all the dependencies

```
npm install
```

### Step 3: Run the project locally

```
npm run dev
```

## Overview

The code is structured in various components:

- navbar
- Home
- Benefits
- OurClasses
- ContactUs
- Footer

### UI representaiton of the web page

![Home](image/README/Home.png)

![Benefits](image/README/Benefits.png)

![ContactUs](image/README/ContactUs.png)

## Conclusion

This project is mainly developed for improving my skills in React, Typescript and also Tailwind-css.
