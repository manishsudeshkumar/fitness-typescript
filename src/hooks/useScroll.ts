import { SelectedPage } from "@/shared/types";
import { useState, useEffect } from "react";

type Props = {
  setSelectedPage: (value: SelectedPage) => void;
  setIsTopOfPage: (value: boolean) => void;
};

const useScroll = ({ setSelectedPage, setIsTopOfPage }: Props) => {
  // const [isTopOfPage, setIsTopOfPage] = useState<boolean>(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY === 0) {
        setIsTopOfPage(true);
        setSelectedPage(SelectedPage.Home);
      } else {
        setIsTopOfPage(false);
      }
    };
    handleScroll();
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);
};
export default useScroll;
