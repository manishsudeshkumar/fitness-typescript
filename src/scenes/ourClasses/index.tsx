import { ClassType, SelectedPage } from "@/shared/types";
import image1 from "@/assets/image1.png";
import image2 from "@/assets/image2.png";
import image3 from "@/assets/image3.png";
import image4 from "@/assets/image4.png";
import { motion } from "framer-motion";
import HText from "@/shared/HText";
import Class from "./Class";

const classes: Array<ClassType> = [
  {
    name: "Weight Training Classes",
    description:
      "Our weight training classes offer a challenging full-body workout designed to increase strength, improve muscle tone, and boost endurance. Led by certified trainers, classes cater to all fitness levels and utilize a variety of equipment for a well-rounded experience.",
    image: image1,
  },
  {
    name: "High Intensity Training Classes",
    description:
      "HIIT classes are fast-paced, full-body workouts that use short bursts of high-intensity exercise to increase endurance and burn calories. Suitable for all fitness levels and led by certified trainers.",
    image: image2,
  },
  {
    name: "Ab Core Classes",
    description:
      "Our Ab Core classes are designed to help you sculpt and strengthen your midsection through a variety of exercises that target your abs and obliques. Led by certified trainers, these classes are suitable for all fitness levels and will help you improve your posture, balance, and overall core strength.",
    image: image3,
  },
  {
    name: "Yoga Classes",
    description:
      "Our Yoga classes offer a peaceful and rejuvenating way to improve flexibility, strength, and balance. Led by experienced instructors, our classes are suitable for all levels, and focus on breathing techniques, mindfulness, and proper alignment. Whether you're new to yoga or a seasoned practitioner, you'll leave our classes feeling relaxed and refreshed.",
    image: image4,
  },
];

type Props = {
  setSelectedPage: (value: SelectedPage) => void;
};

const OurClasses = ({ setSelectedPage }: Props) => {
  return (
    <section id="ourclasses" className="w-full bg-primary-100 py-40">
      <motion.div
        onViewportEnter={() => setSelectedPage(SelectedPage.OurClasses)}
      >
        <motion.div
          className="mx-auto w-5/6"
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.5 }}
          transition={{ duration: 0.5 }}
          variants={{
            hidden: { opacity: 0, x: -50 },
            visible: { opacity: 1, x: 0 },
          }}
        >
          <div className="md:w-3/5">
            <HText>OUR CLASSES</HText>
          </div>
          <p className="py-5">
            We offer a variety of fitness classes including high-intensity
            interval training (HIIT), yoga, dance cardio, and strength training.
            Our classes are designed to improve cardiovascular health, increase
            strength and flexibility, and promote overall wellness.
          </p>
        </motion.div>
        <div className="mt-10 h-[500px] w-full overflow-x-auto overflow-y-hidden">
          <ul className="w-[2000px] whitespace-nowrap">
            {classes.map((item: ClassType, index) => (
              <Class
                key={`${item.name}-${index}`}
                name={item.name}
                description={item.description}
                image={item.image}
              />
            ))}
          </ul>
        </div>
      </motion.div>
    </section>
  );
};

export default OurClasses;
