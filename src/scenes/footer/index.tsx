import Logo from "@/assets/Logo.png";
import { motion } from "framer-motion";
type Props = {};

function Footer({}: Props) {
  return (
    <footer id="footer" className="bg-primary-100 py-16">
      <motion.div
        className="justify-content mx-auto w-5/6 gap-16 md:flex"
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, amount: 0.5 }}
        transition={{ delay: 0.2, duration: 1 }}
        variants={{
          hidden: { opacity: 0, x: -50 },
          visible: { opacity: 1, x: 0 },
        }}
      >
        <div className="mt-16 basis-1/2 md:mt-0">
          <img className="basis-3/5" alt="footer-image" src={Logo} />
          <p className="my-5">
            Our fitness web application is designed to help you stay on track
            with your health and wellness goals, no matter where you are. Built
            with React, Tailwind, Motion Framer, React-Hook Form, and Heroicons,
            our app is fully responsive and optimized for use on desktop,
            tablet, and mobile devices.
          </p>
          <p className="justify-end text-lg italic">
            Designed by MANISH SUDESH KUMAR
          </p>
        </div>
      </motion.div>
    </footer>
  );
}

export default Footer;
