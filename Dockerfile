FROM node:latest

RUN mkdir -p /home/projects/fitness-typescript

COPY . /home/projects/fitness-typescript

WORKDIR /home/projects/fitness-typescript

CMD ["npm","run","dev"]